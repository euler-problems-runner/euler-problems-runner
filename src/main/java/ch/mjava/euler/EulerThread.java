package ch.mjava.euler;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Thread executing an IEulerProblem and and logging the time before start and ending.
 *
 */
public class EulerThread extends Thread
{
    private static final Log log = LogFactory.getLog(EulerThread.class);

    private IEulerProblem eulerProblem;

    public EulerThread(IEulerProblem eulerProblem)
    {
        this.eulerProblem = eulerProblem;
    }

    /**
     * Running the Problem and logging the time.
     *
     */
    @Override
    public void run()
    {
        log.info("start of euler problem execution");
        // use nano-time instead of currentTimeMillis(),
        // provides better and realistic time measurement
        long start = System.nanoTime();
        eulerProblem.execute();
        long end = System.nanoTime();
        log.info("end of euler problem execution");
        long diff = end - start;
        double seconds = (double)diff / 1000000000.0;
        log.info(String.format("It took %d nanoseconds to solve the problem", diff));
        log.info(String.format("This is %s seconds", seconds));
    }
}

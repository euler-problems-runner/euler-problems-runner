package ch.mjava.euler;

/**
 * Interface for executing an Eulerproblem from http://projecteuler.net
 *
 * @author knm
 */
public interface IEulerProblem
{
    public void execute();
}

package ch.mjava.euler.problems;

import ch.mjava.euler.IEulerProblem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Sum up all unique multiples of 3 and 5 from 1 to 100000
 * @author knm
 */
public class EulerProblem1 implements IEulerProblem
{
    private static final Log log = LogFactory.getLog(EulerProblem1.class);

    public void execute()
    {
        long sum = 0;
        for(int i = 3; i < 1000; i++)
        {
            if(i % 3 == 0 || i % 5 == 0)
            {
                sum += i;
            }
        }
        log.info(String.format("Result is : %d", sum));
    }
}

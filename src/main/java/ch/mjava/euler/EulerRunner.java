package ch.mjava.euler;

import ch.mjava.euler.problems.EulerProblem1;

/**
 * Start point to run an eulerProblem. Use 'new' or spring, as you like it
 *
 * @author knm
 */
public class EulerRunner
{
    public static void main(String[] args)
    {
        IEulerProblem problem = new EulerProblem1();
        EulerThread thread = new EulerThread(problem);
        thread.start();
    }
}
